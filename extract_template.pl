#!/usr/bin/perl
#
# extract_template.pl
#
# Create a list of all the debconf template settings contained in a
# given Debian release
#
# Copright 2020 Steve McIntyre <steve@einval.com>
#
# GPLv2+

use strict;
use warnings;
use File::Basename;
use File::stat;
use Getopt::Long;
Getopt::Long::Configure ('no_ignore_case');
Getopt::Long::Configure ('no_auto_abbrev');

my $mirror = "/srv/mirror/debian";
my $suite = "buster";
my $component = "main";
my $arch = "amd64";
my $outfile = "";
my $verbose = 0;
my $verbose_number = 1000;
my $out_fh;
my $result = GetOptions("m|mirror=s"    => \$mirror,
			"suite|s=s"     => \$suite,
			"c|component=s" => \$component,
			"a|arch=s"      => \$arch,
			"o|out=s"       => \$outfile,
			"v|verbose+"    => \$verbose
    ) or die ("Error parsing command line");
my $deb;

if ($outfile eq "") {
    $out_fh = *STDOUT;
}

sub dump_entry {
    my $out_fh = shift;
    my $base = shift;
    my $desc = shift;
    my $tmp = shift;
    my @long_desc = @$tmp;
    my $tmpl_name = shift;
    my $type = shift;
    my $default = shift;
    my $choices = shift;

    if (!defined($default)) {
	if (defined($type)) {
	    if ($type eq "string") {
		$default = "<string>";
	    } elsif ($type eq "select") {
		$default = "<choice>";
	    } elsif ($type eq "multiselect") {
		$default = "<choice(s)>";
	    } elsif ($type eq "password") {
		$default = "<password>";
	    } else {
		$default = "<choice>";
	    }
	} else {
	    print $out_fh "# Unknown type for $base $desc, $default\n";
	    print $out_fh "# Maybe broken template? Guessing...\n";
	    $type = "<choice>";
	    $default = "<choice>";
	}
    }
    if (!defined($desc)) {
	print $out_fh "$base: unknown desc\n";
    }
    if ($type ne "text" and
	$type ne "error" and
	$type ne "title" and
	$type ne "note") {
	print $out_fh "\n### $desc\n";
	foreach my $dl (@long_desc) {
	    print $out_fh "#   $dl\n";
	}
	if (!defined($tmpl_name)) {
	    print $out_fh "# Unknown tmpl_name for $base $desc, $type, $default\n";
	}
	print $out_fh "# d-i $tmpl_name $type $default\n";
	if (defined($choices)) {
	    print $out_fh "# Possible choices: $choices\n";
	}
    }
}

sub process_deb {
    my $out_fh = shift;
    my $deb = shift;
    my $template;
    $template = `dpkg --ctrl-tarfile $deb | tar tf - | grep templates`;
    chomp $template;
    if ($template) {
	my $base = basename($deb);
	print $out_fh "\n############################\n";
	print $out_fh "#### $base\n";
	print $out_fh "############################\n";

	open (TMPL, "dpkg --ctrl-tarfile $deb | tar xf - $template -O |")
	    or die "Failed to extra template from $deb: $!\n";

	my $in_template = 0;
	my $in_descr = 0;
	my $tmpl_name;
	my $type;
	my $default;
	my $desc;
	my $choices;
	my @long_desc;

	while (defined (my $line = <TMPL>)) {
	    chomp $line;
	    if ($line =~ /^Template:\s*(.*)/i) {
		$in_template = 1;
		$tmpl_name = $1;
		$in_descr = 0;
		next;
	    }
	    if ($line =~ /^Type:\s*(.*)/i) {
		$type = $1;
		$in_descr = 0;
		next;
	    }
	    if ($line =~ /^Default:\s*(.*)/i) {
		$default = $1;
		$in_descr = 0;
		next;
	    }
	    if ($line =~ /^Choices:\s*(.*)/i) {
		$choices = $1;
		$in_descr = 0;
		next;
	    }
	    if ($line =~ /^(Description:\s*.*)/i) {
		$in_descr = 1;
		$desc = $1;
		next;
	    }
	    if ($line =~ /^ (.*)/i && $in_descr) {
		push(@long_desc, $1);
		next;
	    }
	    if ($line =~ /^Description-.*/i) {
		$in_descr = 0;
		next;
	    }

	    if ($line =~ /^$/ and $in_template) {
		dump_entry($out_fh, $base, $desc, \@long_desc, $tmpl_name, $type, $default, $choices);
		undef $desc;
		undef @long_desc;
		undef $tmpl_name;
		undef $type;
		undef $default;
		undef $choices;
		$in_template = 0;
	    }
	}
	close TMPL;
	if ($in_template and defined($desc)) {
	    dump_entry($out_fh, $base, $desc, \@long_desc, $tmpl_name, $type, $default, $choices);
	    $in_template = 0;
	}
    }
}

my $num_debs = 0;

if (scalar (@ARGV)) {
    # Ignore the other bits, just use the specified debs
    while ($deb = shift) {
#    print "Processing $deb\n";
#    print "Found control tarball as $control\n";
	process_deb(*STDOUT, $deb);
	$num_debs++;
	if ($verbose && $num_debs % $verbose_number == 0) {
	    print STDERR "Processed $num_debs packages OK\n";
	}
    }
} else {
    my @inputs = ("$mirror/dists/$suite/$component/debian-installer/binary-$arch/Packages.gz",
		  "$mirror/dists/$suite/$component/binary-$arch/Packages.gz");
    my $should_run = 0;

    # If we have an output file, ONLY run if one of the input files is
    # newer
    if ($outfile eq "" || ! -f $outfile) {
	$should_run = 1;
    } else {
	my $sb_out = stat($outfile);
	foreach my $file (@inputs) {
	    if (-f $file) {
		my $sb_in = stat($file);
		if ($sb_in->mtime > $sb_out->mtime)
		{
		    $should_run = 1;
		}
	    }
	}
    }

    if ($should_run) {
	if ($outfile ne "") {
	    open($out_fh, ">", "$outfile.new")
		or die "Can't open $outfile.new for writing: $!\n";
	}
	print $out_fh "#_preseed_V1\n";
	foreach my $file (@inputs) {
	    if ($verbose) {
		print STDERR "Checking for debs in $file\n";
	    }
	    if (-f $file) {
		open(PACKAGES, "zcat -f $file |")
		    or die "Can't read Packages file $file: $!\n";
		while (defined (my $line = <PACKAGES>)) {
		    chomp $line;
		    if ($line =~ m/^Filename: (.*)/) {
			process_deb($out_fh, "/$mirror/$1");
			$num_debs++;
			if ($verbose >= 2) {
			    print STDERR "Processed $1\n";
			} elsif ($verbose && $num_debs % $verbose_number == 0) {
			    print STDERR "Processed $num_debs packages OK\n";
			}
		    }
		}
		close PACKAGES;
		if ($verbose) {
		    print STDERR "Processed $num_debs packages OK\n";
		}
	    }
	}
	if ($outfile ne "") {
	    close $out_fh;
	    rename "$outfile.new", "$outfile";
	}
    } else {
	if ($verbose) {
	    print STDERR "Output file $outfile is newer than input files, not running\n";
	}
    }
}

