#!/bin/bash
#
# run-mail
#
# Do stuff with mail depending on the state of the network. This
# script should be run every minute, and it will do different stuff
# depending on the state of the network connection we have.
#
# The 5 states we recognise are:
# a) no network
# b) networked through ppp*
# c) networked through eth* to a machine that's doing dialup (hard to detect)
# d) networked directly through eth* with a fast connection to lump
# e) on the same local network as lump
#
# In a) we clearly should not be attempting to do mail over the network
#
# In b) and c) we should only run occasionally:
#       every ten minutes sync inbox(es)
#       every minute flush the bsmtp queue
#       every 4 hours (240 minutes) for mail folders
#
# In d) we have easy and cheap net access, so:
#       every few minutes sync inbox(es)
#       every minute flush the bsmtp queue
#       every 2 hours (120 minutes) for mail folders
#
# In e) we have free net access to lump, so:
#       every minute sync inbox(es)
#       every minute flush the bsmtp queue
#       every 1 hour (60 minutes) for mail folders

#set -x

#####################
# Settings
#####################

# The name of the mail host we use, for both imap sync and outgoing
# bsmtp
MAILHOST=cheddar.einval.com
MAILHOST_LOCAL=10.13.0.1
CONFTAB=/etc/run-mail.conf
LOG=/tmp/run-mail.log

# The time in ms for round-trip ping reply before we assume we're on a
# slow network
SLOW_PING=200
NO_PING=99999

# Default debug level
VERBOSE=0

vecho ()
{
        if [ $VERBOSE -gt 0 ] ; then
                echo "$@"
        fi
}

vvecho ()
{
        if [ $VERBOSE -gt 1 ] ; then
                echo "$@"
        fi
}

vvvecho ()
{
        if [ $VERBOSE -gt 2 ] ; then
                echo "$@"
        fi
}

# Calculate an average ping time to the specified host, in ms.
time_ping () {
    NUM_PINGS=5
    time=`fping -qn -c5 -r1 -p1000 -B1.0 $1 2>&1 | \
            awk -F/ '/min\/avg\/max/ {printf("%d\n",$8)}'`
    if [ "$time"x = ""x ] ; then
        time=$NO_PING
    fi
    echo $time
    vecho "time_ping: time to $1 is $time" >> $LOG
}

# Try to detect the network we have. Find the interface that we use
# for our default route. If this is empty, we do not have a
# network. Easy - we're in a) above. If we have a ppp connection, then
# that's also easy - we're in b). To distinguish c) and d), we need to
# do some timings. Do a couple of pings to lump and check the
# round-trip times. Anything over (say) 300 ms is too slow to be
# useful, so assume c). If the ping return is < 1 ms, we're local -
# e). Else d).
connection_type () {
    ROUTE=`/sbin/route -n | awk '/^0\.0\.0\.0/ {print $8; exit}'`
    case "$ROUTE"x in
        ppp*)
            # Slow modem link
            NET="SLOW"
            ;;
        wlan*|eth*|ath*|tun*)
            # Ethernet or VPN, which *might* be fast. Do a further check...
            PING_TIME=`time_ping $MAILHOST`
            if [ $PING_TIME -eq $NO_PING ] ; then
                NET="NONE"
            elif [ $PING_TIME -gt $SLOW_PING ] ; then
                NET="SLOW"
            elif [ $PING_TIME -gt 0 ] ; then
                NET="FAST"
	    fi
            PING_TIME=`time_ping $MAILHOST_LOCAL`
            if [ $PING_TIME -ne $NO_PING ]; then
                NET="LOCAL"
            fi
            IP=`/sbin/ifconfig $ROUTE | awk '/inet addr/ {gsub("inet addr:",""); print $1}'`
            case $IP in
                10.0.24*)
                    vecho "HACK - at Swansea, assuming FAST"
                    NET="FAST" # Swansea
		    ;;
                10.21.6.*)
                    vecho "HACK - at BCS, assuming FAST"
                    NET="SLOW" # BCS
		    ;;
                10.176.*)
                    vecho "HACK - at Surewest, assuming FAST"
                    NET="FAST" # Surewest
                    ;;
            esac
            ;;
        x*|*)
            # No route
            NET="NONE"
            ;;
    esac
    echo $NET
    vecho "connection_type: connection to $MAILHOST is $NET" >> $LOG
}

day_minute () {
    minute=`date "+%H*60+%M" | bc`
    echo $minute
}

divide_minute () {
    minute=`echo $1 % $2 | bc`
    echo $minute
}

# main

while [ $# -gt 0 ]
do
    if [ $1 = "-v" ] ; then
        VERBOSE=$(($VERBOSE + 1))
    else
        echo "$0: Unknown option $1"
    fi
    shift
done

NET=`connection_type`

vecho "NET is $NET"

MINUTE=`day_minute`
while true
do
    read SPEED OFFSET MINUTES USER SCRIPT
    if [ $? -ne 0 ] ; then
        break
    fi
    vvecho SPEED $SPEED
    vvecho OFFSET $OFFSET
    vvecho MINUTE $MINUTE
    vvecho MINUTES $MINUTES
    vvecho USER $USER
    vvecho SCRIPT \"$SCRIPT\"

    case "$SPEED"x in

        "#"*x|""x) # Comment - do nothing
            vvvecho "Comment; doing nothing"
        ;;

        LOCALx|FASTx|SLOWx|NONEx)
            if [ $MINUTE -ge $OFFSET ] ; then
                USE_MINUTE=$(($MINUTE - $OFFSET))
                DIV_MINUTE=`divide_minute $USE_MINUTE $MINUTES`
                if [ "$NET"x = "$SPEED"x ] ; then
                    if [ $DIV_MINUTE -eq 0 ] ; then
                        if [ "$USER"x = "-"x ] ; then
                            vecho "Running $SPEED $OFFSET $MINUTES $SCRIPT"
                            $SCRIPT &
                        else
                            vecho "Running $SPEED $OFFSET $MINUTES sudo -u $USER $SCRIPT"
                            sudo -u $USER $SCRIPT &
                        fi
                    else
                        vecho "Not running $SPEED $OFFSET $MINUTES $SCRIPT - MINUTE $USE_MINUTE, MINUTES $MINUTES, DIV_MINUTE $DIV_MINUTE"
                    fi
                fi
            fi
        ;;
    esac
done
