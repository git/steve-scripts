#!/bin/sh

if [ "$1"x = ""x ] ; then
	exit 1
fi

if [ "$1"x = "-t"x ] ; then
    THREADS=1
    shift
fi

case `uname` in

	AIX|SunOS)
		OPTS=-fade;;
	NetBSD)
		OPTS=-wwwaux;;
	Linux)
		OPTS=-fade
        if [ "$THREADS"x = "1"x ] ; then
            OPTS=${OPTS}L
        fi
        ;;
	FreeBSD|Osprey|OSprey)
		OPTS=wwwaux;;
	*)
		echo Warning! $0 does not know about OS `uname`...
		exit 1;;
esac

ps $OPTS | grep "$@" | grep -v "$0" | grep -v grep
