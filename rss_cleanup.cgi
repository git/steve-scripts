#!/bin/sh

if [ "$QUERY_STRING"x = ""x ] ; then
    QUERY_STRING="$1"
fi

case "$QUERY_STRING" in
    "http://www.highways.gov.uk/rssfeed/"*)
        SITE=highways;;
    "http://rss.slashdot.org"*)
        SITE=slashdot;;
esac

if [ "$SITE"x = "highways"x ] ; then
    WANTED_WORDS="Cambridgeshire M11 A14"
    echo "Content-Type: text/xml; charset=utf-8"
    echo ""

    wget -q -O - "$QUERY_STRING" | gawk -v WANTED_WORDS="$WANTED_WORDS" '
    BEGIN       {
                    a = split(WANTED_WORDS, WANTED)
#                    printf("%d words wanted\n", a)
                 }
    /<item>/    {
                    item = sprintf("%s\n", $0)
                    item_wanted = 0
                    in_item = 1
                    next
                }
    /<title>/   {
                    if (in_item) {
                        item = sprintf("%s%s\n", item, $0)
                        for (i in WANTED) {
#                            printf("Looking for %s, looking at %s\n", WANTED[i], $0)
                            if (match ($0, WANTED[i])) {
                                  item_wanted = 1
                                next
                            }
                        }
                    } else {
                        print $0
                        next
                    }
                }
    /<\/item>/  {
                    item = sprintf("%s%s\n", item, $0)
                    if (item_wanted) {
                        print item
                    }
                    in_item = 0
                    next
                }
    /.*/        {
                    if (in_item) {
                        item = sprintf("%s%s\n", item, $0)
                    } else {
                        print $0
                    }
                }'
elif [ "$SITE"x = "slashdot"x ] ; then
    echo "Content-Type: text/xml; charset=utf-8"
    echo ""
    wget -q -O - "$QUERY_STRING" | gawk '
    /<\/description>/  { gsub("^.*;", "") }
    /.*/               { print $0 }'
else
    echo "Content-Type: text/plain; charset=utf-8"
    echo ""
    echo "ERROR! No valid feed requested!"
    echo "You asked for:"
    echo $QUERY_STRING
fi
