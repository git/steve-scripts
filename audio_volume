#!/bin/sh
#
# Script to manage audio buttons on thinkpad
#

INPUT="$@"

if [ "$INPUT"x = ""x ]; then
    INPUT=state
fi

SINK="$(pactl list short sinks | awk '/RUNNING/ {print $2}')"
SOURCE="$(pactl list sources | awk '
   /Name: / {name=$2}
   /Description: Tiger Lake.*Headphone/ {print name}')"
FULL_VOLUME=99999

# Call pactl and parse the output. There are two volume levels, for
# left and right. Assume we're balanced and just read the left value
sink_volume () {
    pactl list sinks | awk "
    /Name: $SINK/ { this_sink=1 }
    /Volume:/ { if (this_sink) { vol=\$3; print vol; this_sink=0 } }
    "
}

case "$INPUT" in
    --help)
	echo "$0: control volume settings"
	echo "$0: state|+N|-N|mute"
	;;
    +*|-*)
	# Volume up/down
	OLD_LEVEL=$(sink_volume)
	CHANGE=$(($INPUT * $FULL_VOLUME / 100))
	NEW_LEVEL=$(($OLD_LEVEL + $CHANGE))
	if [ $NEW_LEVEL -gt $FULL_VOLUME ]; then
	    NEW_LEVEL=$FULL_VOLUME
	fi
	if [ $NEW_LEVEL -lt 0 ]; then
	    NEW_LEVEL=0
	fi
	pactl set-sink-volume "$SINK" "$NEW_LEVEL"
	echo "$0: sink $SINK, got $INPUT, old audio level $OLD_LEVEL, setting audio level $NEW_LEVEL"
	;;
    mute)
	# Toggle output mute on/off
	echo "$0: sink $SINK, got $INPUT, toggling output mute"
	pactl set-sink-mute "$SINK" toggle
	;;
    micmute)
	# Toggle input mute on/off
	echo "$0: sink $SINK, got $INPUT, toggling mic mute"
	pactl set-source-mute "$SOURCE" toggle
	;;
    state)
	# Just print the current level
	LEVEL=$(sink_volume)
	echo "$0: sink $SINK, got audio level $LEVEL"
	;;
    *)
	echo "$0: Unknown argument(s): $INPUT"
	exit 1
	;;
esac
