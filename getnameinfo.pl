#!/usr/bin/perl -w

use strict;
use Socket;
use Socket6 qw(getnameinfo NI_NAMEREQD inet_pton);
use URI;
use LWP::UserAgent;
use JSON;
use utf8;
use Net::hostent;
use Scalar::Util qw(isdual);

my $host = shift;
my $hostname;
my $result = "";
my $score = 0;
my $addr;
my $sockaddr;
my $err;

if ($host =~ /:/) {
    # IPv6
    $addr = inet_pton(AF_INET6, $host) or die "Failed to convert: $!\n";
    $sockaddr = sockaddr_in6(0, $addr);
} else {
    # IPv4
    $addr = inet_pton(AF_INET, $host) or die "Failed to convert: $!\n";
    $sockaddr = sockaddr_in(0, $addr);
}

($hostname) = getnameinfo($sockaddr, NI_NAMEREQD);

if (isdual($hostname)) {
    $hostname = "no rDNS";
}

print "$host -> $hostname\n";

