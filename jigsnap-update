#!/usr/bin/perl
#
# Copyright 2019? Phil Hands <phil@hands.com>
# 
# Tweaks since by Steve McIntyre <steve@einval.com>

use Set::Tiny;
use File::Spec;
use Cwd 'abs_path';

# lifted from mkjigsnap
use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use File::Find;
use File::Copy;
use File::Path qw(make_path);
use Compress::Zlib;
use IO::Uncompress::Gunzip qw(gunzip $GunzipError) ;
Getopt::Long::Configure ('no_ignore_case');
Getopt::Long::Configure ('no_auto_abbrev');

my $dryrun = 0;
my $verbose = 0;
#my $startdate = `date -u`;
my @keywords ; #   = ["Debian"];
my @mirrors ;
my ($dirname, $failedfile, $ignorefile, $jigdolist, $mirror, $cdname,
    $outdir, $tempdir, $template, $check_checksums, $checksum_out, $backref_file, $hash_cache_file);
my $result;
my @failed_files;

my $ignored = 0;
my $redundant_deleted = 0;
my $failed = 0;
my $new_links = 0;
my $extant_total = 0;
my $desired_total = 0;

# FIXME: probably ought to check the $result here
$result = GetOptions(#"b=s" => \$backref_file,
                     #"c"   => \$check_checksums,
                     #"C=s" => \$checksum_out,
                     "d=s" => \$dirname,
                     "f=s" => \$failedfile,
                     #"h=s" => \$hash_cache_file,
                     #"H"   => \$hash_cache_update,
                     "i=s" => \$ignorefile,
                     #"J=s" => \$jigdolist,
                     #"j=s" => \@jigdos,
                     "k=s" => \@keywords,
                     "m=s" => \@mirrors,
                     "N"   => \$dryrun,
                     #"n=s" => \$cdname,
                     "o=s" => \$outdir,
                     #"T=s" => \$tempdir,
                     #"t=s" => \$template,
                     "v"   => \$verbose);

my $cur_dir = undef ;
my $desired = Set::Tiny->new;
my $extant = Set::Tiny->new;
my $ignored_fails = Set::Tiny->new;


if (defined($failedfile)) {
    open(FAIL_LOG, "> $failedfile") or die "Failed to open $failedfile: $!\n";
}

test_needed_perms();

sub test_needed_perms {
    my $testfile = "$dirname/.testfile";
    # First, check we can write to the output dir
    unlink($testfile);
    open(TESTFILE, "> $testfile")
	or die "Failed to open $testfile for writing, don't have permissions";
    close(TESTFILE);
    foreach my $mirror(@mirrors) {
	# We expect that the mirror will include "README", let's use
	# that as a link source
	my $testsrc = "$mirror/README";
	unlink($testfile);
	if (-f $testsrc) {
	    link($testsrc, $testfile)
		or die "Failed to link $testsrc to $testfile: $!";
	}
    }
    unlink($testfile);
    print "Permissions look good\n";
}

sub parse_ignore_file {
    my $inputfile = shift;
    my $num_ignored_loaded = 0;
    open(INLIST, "$inputfile") or return;
    while (defined (my $pkg = <INLIST>)) {
        chomp $pkg;
        $ignored_fails->insert($pkg);
        $num_ignored_loaded++;
    }
    #printf "parse_ignore_file: loaded %d entries from file %s, resulting in a set of %d items\n", $num_ignored_loaded, $inputfile, $ignored_fails->size;
    close(INLIST);
}
if (defined($ignorefile)) {
    parse_ignore_file($ignorefile);
}


sub populate_set_from_dir {
    my ($set, $keyword, $cur_dir) = @_ ;
    my $dir = File::Spec->catfile($dirname, $keyword, $cur_dir);

    opendir(DIR, $dir) or return;
    
    $set->insert( grep {
	            /^[^.]/          # no dot-files
		    && -f "$dir/$_"  # and is a file
		  } readdir(DIR) );
    closedir(DIR);
}

sub handle_dir {
    my ($keyword, $dir, $set) = @_ ;
    populate_set_from_dir($extant, $keyword, $dir) ;
    if (!$set->is_equal($extant)) {
	if ($verbose && !$set->difference($extant)->is_empty) {
	    print "missing: $dir " . $set->difference($extant)->as_string . "\n" ;
#	    print "set:    " . $set->as_string . "\n";
#	    print "extant: " . $extant->as_string . "\n";
	}
	
	my $outdir = File::Spec->catdir($dirname, $keyword, $dir);
	if (! -d $outdir) {
	    if (!$dryrun) {
		make_path($outdir) ;
	    }
	    else {
		print "DRYRUN: would try to make_path('$outdir')\n";
	    }
	}

	foreach my $filename ($set->difference($extant)->members) {
	    if ($verbose) {
		print "Looking at $dir/$filename\n";
	    }
	    my $relpath = File::Spec->catfile($dir, $filename);
	    my $outfile = File::Spec->catfile($outdir, $filename);
	    
	    my $link;
	    my $link_ok = 0;
	    my $infile;
	    
	    foreach my $mirror (@mirrors) {
		$infile = File::Spec->catfile($mirror, $relpath);
		
		if (-l $infile) {
		    if ($verbose) {
			print "infile ($infile) is a link, ";
		    }
		    $infile = abs_path($infile);
		    if ($verbose) {
			print "use ($infile) instead\n";
		    }
		}
		if ($verbose) {
		    print "look for $relpath:\n";
		}
		if (!$dryrun) {
		    if ($verbose) {
			print "  try $infile\n";
		    }
		    if (link ($infile, $outfile)) {
			$link_ok = 1;
			if ($verbose) {
			    print "    SUCCESS\n";
			}
			last;
		    } else {
			if ($verbose) {
			    print "    link($infile, $outfile) FAILED: $!\n";
			}
		    }
		} else {
		    print "DRYRUN: would try to linking '$infile' to '$outfile'\n";
		    $link_ok = 1;
		    last;
		}
	    }
	    
	    if ($link_ok == 0) {
		if ($ignored_fails->contains($relpath)) {
		    $ignored++;
		} else {
		    if (defined($failedfile)) {
			print FAIL_LOG $relpath . "\n" ;
		    }
		    else {
			# No logfile, print to stdout then
			print "\nFailed to create link $outfile\n";
		    }
		    $failed++;
		}
	    } else {
		$new_links++;
		$extant->insert($filename);
		if ($ignored_fails->contains($relpath)) {
		    print "\n$relpath marked as failed, but we found it anyway!\n";
		}
	    }
	}
	
	if (!$extant->difference($set)->difference($ignored_fails)->is_empty) {
	    foreach my $filename ($extant->difference($set)->difference($ignored_fails)->members) {
		if (!$dryrun) {
		    if ($verbose) {
			print "  removing $outdir/$filename\n";
		    }
		    unlink ("$outdir/$filename") || die "Failed to unlink $outdir/$filename : $!\n" ;
		    $redundant_deleted++;
		    $extant->remove($filename);
		} else {
		    print "DRYRUN: would unlink $outdir/$filename\n";
		}
	    }
	    rmdir $outdir ; # try removing it, just in case it's now empty
	}
    }
    
    $extant_total += $extant->size;
    $desired_total += $set->size;
    $extant->clear ;
}

my $old_depth = -1 ;
my $cur_depth ;
my @keyword_stack ;
my @curdir_stack ;
my @desired_stack ;

sub process_line {
    my ($keyword, $dname, $fname) = @_ ;

    if (defined($keyword)) {
	$cur_depth = () = $dname =~ m#/#g;
    } else {
	$cur_depth = -1 ;
    }

    # if we've gone up in depth, time to process dir(s)
    while ($cur_depth < $old_depth) {
	if (defined($desired_stack[$old_depth])) {
#	    printf "dir: %s desired: %s\n", $curdir_stack[$old_depth], $desired_stack[$old_depth]->as_string ;
	    handle_dir($keyword_stack[$old_depth], $curdir_stack[$old_depth], $desired_stack[$old_depth]) ;
	    $keyword_stack[$old_depth] = undef ;
	    $curdir_stack[$old_depth] = undef ;
	    $desired_stack[$old_depth] = undef ;
	}
	$old_depth-- ;
    }

    return if (-1 == $cur_depth) ; # we're in the last invocation, so that's enough
    
    # if we've gone down the tree (or just started) instantiate a new set
    # also, if we've wandered up into a new level, in the above while, act similarly
    if ($cur_depth > $old_depth || !defined($curdir_stack[$cur_depth])) {
	$keyword_stack[$cur_depth] = $keyword ;
	$curdir_stack[$cur_depth] = $dname ;
	$desired_stack[$cur_depth] = Set::Tiny->new($fname) ;
	$old_depth = $cur_depth ;
    }
    else {
	if ($curdir_stack[$cur_depth] ne $dname || $keyword_stack[$cur_depth] ne $keyword) {
#	    printf "dir: %s desired: %s\n", $curdir_stack[$cur_depth], $desired_stack[$cur_depth]->as_string ;
	    handle_dir($keyword_stack[$cur_depth], $curdir_stack[$cur_depth], $desired_stack[$cur_depth]) ;
	    $desired_stack[$cur_depth]->clear ; # not sure we need this, but seems prudent
	    $keyword_stack[$cur_depth] = $keyword ;
	    $curdir_stack[$cur_depth] = $dname ;
	    $desired_stack[$cur_depth] = Set::Tiny->new($fname) ;
	}
	else {
	    $desired_stack[$cur_depth]->insert($fname) ;
	}
    }
}

while (<<>>) {
    chomp ;

    if (my ($checksum, $keyword, $dname, $fname) = m#^([^=]*)=([^:]*):(.*)/([^/]*)$#) {
	next if ($keyword ne "Debian") ; # FIXME:  mkjigsnap uses $keywords here, should we still?

	# I'll leave the option to have other $keyword possibilites here anyway,
	# rather than scattering 'Debian' about the place
	process_line($keyword, $dname, $fname) ;
    }
    else {
	printf STDERR "%s: Error: while trying to parse [%s] (which is line no:%d)\n", $0, $_, ${\*ARGV}->input_line_number ;
    }
}
process_line(undef) ; # one last call to flush the final dir.

printf STDERR "STATS:\n";
printf STDERR "  % 8d old files removed\n", $redundant_deleted;
printf STDERR "  % 8d new snapshot links added\n", $new_links;
printf STDERR "  % 8d files desired in the snapshot\n", $desired_total;
printf STDERR "  % 8d files now in the snapshot\n", $extant_total;
printf STDERR "  % 8d files marked as ignored\n", $ignored;
printf STDERR "  % 8d attempted snapshot links failed\n", $failed;
if ($desired_total != ($extant_total + $ignored + $failed)) {
    printf STDERR "Numbers don't add up...!\n";
}
if ($failed > 0 && defined ($failedfile)) {
    printf STDERR "Failed links listed in $failedfile\n";
}

