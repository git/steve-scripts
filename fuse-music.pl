#!/usr/bin/perl
#
# fuse-music.pl
#
# Simple transcoding filesystem for converting FLAC files into MP3 or Ogg Vorbis files
#
# GPL v2
#
# Copyright (c) 2010 Steve McIntyre <steve@einval.com>

use strict;
use Filesys::Statvfs;
use Fuse;
use IO::File;
use POSIX qw(ENOENT EROFS ENOSYS EEXIST EPERM EBUSY O_RDONLY O_RDWR O_APPEND O_CREAT);
use Fcntl qw(O_RDWR O_WRONLY);
use Audio::FLAC::Header;
use Getopt::Long;
use DB_File;
use File::Temp qw/ tempfile tempdir /;

my $debug = 0;

my %formats;
my $format_choice = "ogg"; # Default

my %size_array;
my $format = "";
my $quality = 0;
my $base_dir = "";
my $mountpoint = "";
my $size_cache_db = "";
my $current_file;
my $current_file_size = 0;
my $current_file_in_use = 0;
my $membuf;
my $read_size = 32768;
open(MEMORY,'+>', \$membuf)
    or die "Can't open memory file: $!";

sub fixup {
    my $file = shift;
    $file =~ s/\.$formats{$format_choice}{"extension"}$/.flac/;
    return $base_dir . $file
}

sub file_size {
	my ($file) = shift;
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
        $atime,$mtime,$ctime,$blksize,$blocks)
        = lstat($file);
    if (! $dev) {
        return -$!;
    }
    return $size;
}

sub file_mtime {
	my ($file) = shift;
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
        $atime,$mtime,$ctime,$blksize,$blocks)
        = lstat($file);
    if (! $dev) {
        return -$!;
    }
    return $mtime;
}

sub encode_file {
    my ($file) = shift;
    my ($title, $artist, $album, $date, $track, $genre);
    my $bytes_read = 0;
    my $buf;
    my $encode = $formats{$format_choice}{"encode"};
    my $options;
    my ($fh, $filename);

    if ($current_file_in_use) {
        if (! (($current_file eq "") or ($current_file eq $file))) {
            return -EBUSY();
        }
    }

    $current_file = $file;
    $current_file_size = 0;
    $current_file_in_use = 1;
    truncate(MEMORY, 0);
    seek(MEMORY, 0, 0);
    my $escaped_file = quotemeta($current_file);

    # If the encoder can't read FLACs directly, will need to extract
    # tags so we can pass them in to the encoder later
    if (! $formats{$format_choice}{"directflac"}) {
        my $flac = Audio::FLAC::Header->new($file) or return -$!;
        my $tags = $flac->tags() or return -$!;
        $title = quotemeta($tags->{TITLE});
        $artist = quotemeta($tags->{ARTIST});
        $album = quotemeta($tags->{ALBUM});
        $date = quotemeta($tags->{DATE});
        $track = quotemeta($tags->{TRACKNUMBER});
        $genre = quotemeta($tags->{GENRE});
        $options = $formats{$format_choice}{"opt_tags"}($title,$artist,$album,$date,$track,$genre);
        $options = $options . " " . $formats{$format_choice}{"opt_qual"}($quality);
        $options = $options . " " . $formats{$format_choice}{"opt_files"};
        if (! $formats{$format_choice}{"stdout"}) {
            ($fh, $filename) = tempfile();
            $options = $options . " $filename";
            $encode = $formats{$format_choice}{"encode"} . " " . $options;
            my $command = "flac -scd $escaped_file | $encode";
            if ($debug) {
                print "Calling $command\n";
            }
            system "$command";
            open(ENCREAD, '<', "$filename")
                or print "Failed to open, $!\n";
        } else  {
            my $command = "flac -scd $escaped_file | $encode |";
            if ($debug) {
                print "Calling $command\n";
            }
            open(ENCREAD, $command)
                or print "Failed to open, $!\n";
        }
    } else {    
        $options = $options . " " . $formats{$format_choice}{"opt_qual"}($quality);        
        $options = $options . " " . $formats{$format_choice}{"opt_files"};
        if (! $formats{$format_choice}{"stdout"}) {
            ($fh, $filename) = tempfile();
            $options = $options . " $filename";
            $encode = $formats{$format_choice}{"encode"} . " " . $options;
            if ($debug) {
                print "Calling $encode $escaped_file\n";
            }
            system "$encode $escaped_file";
            open(ENCREAD, '<', "$filename")
                or print "Failed to open, $!\n";
        } else {
            $encode = $formats{$format_choice}{"encode"} . " " . $options;
            if ($debug) {
                print "Calling $encode $escaped_file |\n";
            }
            open(ENCREAD, "$encode $escaped_file |")
                or print "Failed to open, $!\n";
        }
    }
    
    while (($bytes_read = read(ENCREAD, $buf, $read_size)) > 0) {
        $current_file_size += $bytes_read;
        print MEMORY $buf;
    }
    if ($debug) {
        print "Read $current_file_size bytes from encoder\n";
    }
	close(ENCREAD);
    if (! $formats{$format_choice}{"stdout"}) {
        unlink($filename);
    }
    $current_file_in_use = 0;
    return 0;
}

sub oggenc_qual {
    my $quality = shift;
    my $options = "";
    if (length($quality)) {
        $options = $options . " -q $quality";
    }
    return $options;
}    

sub lame_tags {
    my $title = shift;
    my $artist= shift;
    my $album = shift;
    my $date = shift;
    my $track = shift;
    my $genre = shift;
    my $options = "";
    if (length($title)) {
        $options = $options . " --tt $title";
    }
    if (length($artist)) {
        $options = $options . " --ta $artist";
    }
    if (length($album)) {
        $options = $options . " --tl $album";
    }
    if (length($date)) {
        $options = $options . " --ty $date";
    }
    if (length($track)) {
        $options = $options . " --tn $track";
    }
    if (length($genre)) {
        $options = $options . " --tg $genre";
    }
    return $options;
}

sub lame_qual {
    my $quality = shift;
    my $options = "";
    if (length($quality)) {
        $options = $options . " -V $quality";
    }
    return $options;
}    

sub x_getsize {
    my ($file) = shift;
    my $key_size = "SIZE-$format_choice-$quality" . $file;
    my $key_time = "MTIME-$format_choice-$quality" . $file;
    my $size = 0;
    my $mtime = file_mtime($file);
    my $cached_mtime;
    # Do we have the file size in the DB?
    $size = $size_array{"$key_size"};
    $cached_mtime = $size_array{"$key_time"};
    if ($size) {
        if ($cached_mtime && ($mtime <= $cached_mtime)) {
            if ($debug) {
                print "$file: returning valid cached size $size from hash db\n";
            }
            return $size;
        } else {
            if ($debug) {
                print "$file: have a cached size $size from hash db, but it's out of date\n";
            }
        }
    } else {
        # Nope, we'll have to work it out
        if ($debug) {
            print "$file: don't have a cached size for $key_size, encoding now\n";
        }
    }

    my $ret = encode_file($file);
    if ($ret != 0) {
        return $ret;
    }
    $size_array{"$key_size"} = $current_file_size;
    $size_array{"$key_time"} = $mtime;
    if ($debug) {
        print "$file: caching size $current_file_size for $key_size, mtime $mtime\n";
    }
    return $current_file_size;
}

sub x_getattr {
	my ($file) = fixup(shift);
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
        $atime,$mtime,$ctime,$blksize,$blocks)
        = lstat($file);
    if (! $dev) {
        return -$!;
    }
    if ($file =~ /\.flac$/) {
        $size = x_getsize($file);
        if ($debug) {
            print "encoded size $size bytes\n";
        }
    }
	return ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
            $atime,$mtime,$ctime,$blksize,$blocks);
}

sub x_getdir {
	my ($dirname) = fixup(shift);
	unless(opendir(DIRHANDLE,$dirname)) {
		return -ENOENT();
	}
    my @outfiles;
	my (@infiles) = readdir(DIRHANDLE);
    foreach my $file(@infiles) {
        if ($file =~ /\.flac$/) {
            $file =~ s/\.flac$/.$formats{$format_choice}{"extension"}/;
        }
        if ($file !~ $size_cache_db) {
            push @outfiles, $file;
        }
    }
	closedir(DIRHANDLE);
	return (@outfiles, 0);
}

sub x_open {
	my ($file) = fixup(shift);
	my ($mode) = shift;
    if ($mode & (O_RDWR|O_WRONLY)) {
        return -EROFS();
    }
    if (! ($file =~ /\.flac$/)) {
        return -$! unless sysopen(FILE,$file,$mode);
        if ($debug) {
            print "Non-flac, so reading normally\n";
        }
        close(FILE);
        return 0;
    }

    return encode_file($file);
}

sub x_read {
	my ($file,$bufsize,$off) = @_;
	my $rv = -ENOSYS();
    my $bytes_read;
    $file = fixup($file);
    if (! ($file =~ /\.flac$/)) {
        -e $file or return -ENOENT();
        my ($fsize) = -s $file;
        open (FILE, '<', $file) or return $!;
        if(seek(FILE,$off,SEEK_SET)) {
            $bytes_read = read(FILE, $rv, $bufsize);
            if ($debug) {
                print "$bytes_read/$bufsize bytes of raw data read from file $file\n";
            }
        }
        close(FILE);
    } else {
        if(seek(MEMORY,$off,SEEK_SET)) {
            $bytes_read = read(MEMORY, $rv, $bufsize);
            if ($debug) {
                print "$bytes_read bytes of transcoded data read from file\n";
            }
        }
    }
	return $rv;
}

sub x_release {
    my ($file) = fixup(shift);
    if ($file eq $current_file) {
        $current_file = "";
        $current_file_size = 0;
        truncate(MEMORY, 0);
        seek(MEMORY, 0, 0);
    }
    return 0;
}

sub x_readonly {
    return -EROFS();
}

sub err { return (-shift || -$!) }

sub x_readlink { return readlink(fixup(shift)                 ); }

sub x_statfs {
	my($bsize, $frsize, $blocks, $bfree, $bavail,
		$files, $ffree, $favail, $flag, $namemax) = Filesys::Statvfs::statvfs("$base_dir");
	return ($namemax, $files, 0, $blocks, 0, $bsize);
}

$formats{"ogg"}{"extension"}  = "ogg";
$formats{"ogg"}{"def_qual"}   = 7;
$formats{"ogg"}{"encode"}     = 'oggenc -Q';
$formats{"ogg"}{"opt_tags"}   = "";
$formats{"ogg"}{"opt_qual"}   = \&oggenc_qual;
$formats{"ogg"}{"opt_files"}  = "-o -";
$formats{"ogg"}{"directflac"} = 1;
$formats{"ogg"}{"stdout"}     = 1;

$formats{"mp3"}{"extension"}  = "mp3";
$formats{"mp3"}{"def_qual"}   = 4;
$formats{"mp3"}{"encode"}     = "lame --quiet";
$formats{"mp3"}{"opt_tags"}   = \&lame_tags;
$formats{"mp3"}{"opt_qual"}   = \&lame_qual;
$formats{"mp3"}{"opt_files"}  = "-";
$formats{"mp3"}{"directflac"} = 0;
$formats{"mp3"}{"stdout"}     = 0;

GetOptions('format=s' => \$format_choice,
           'quality=i' => \$quality,
           'debug=i' => \$debug);
$base_dir = shift(@ARGV) if @ARGV;
$mountpoint = shift(@ARGV) if @ARGV;

# We only know how to do ogg and mp3 yet!
if (!(($format_choice eq "ogg") or ($format_choice eq "mp3"))) {
    die "Invalid output format choice $format_choice\n";
}

# Use default quality level for the encoder if nothing is specified
if ($quality == 0) {
    $quality = $formats{$format_choice}{"def_qual"};
}

$size_cache_db = ".sizes-$format_choice-$quality";

if (!defined($base_dir) or !defined($mountpoint)) {
    die "Need a base directory and a mountpoint!\n";
}

tie %size_array, 'DB_File', "$base_dir/$size_cache_db";
Fuse::main(
	mountpoint=>$mountpoint,
	getattr =>"main::x_getattr",
	readlink=>"main::x_readlink",
	getdir  =>"main::x_getdir",
	mknod   =>"main::x_readonly",
	mkdir   =>"main::x_readonly",
	unlink  =>"main::x_readonly",
	rmdir   =>"main::x_readonly",
	symlink =>"main::x_readonly",
	rename  =>"main::x_readonly",
	link    =>"main::x_readonly",
	chmod   =>"main::x_readonly",
	chown   =>"main::x_readonly",
	truncate=>"main::x_readonly",
	utime   =>"main::x_readonly",
	write   =>"main::x_readonly",
	open    =>"main::x_open",
	release =>"main::x_release",
	read    =>"main::x_read",
	statfs  =>"main::x_statfs",
	threaded=>0,
	debug => $debug,
    mountopts =>"allow_other",
);
