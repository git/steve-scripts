#!/usr/bin/perl
#
# pdu-control
#
# GPL v2+ (c) Steve McIntyre 2018
#
# Simple script to interact with APC PDUs via SNMP to show status and
# turn named ports on and off. Can even use regexps if desired
#
# Depends on the following Debian packages:
# libconfigreader-simple-perl snmp 
#
# Also needs the APC PowerNet MIB from
# http://www.apc.com/us/en/tools/download/download.cfm?sw_sku=SFPMIB426&software_id=MFOI%2DAVBQP3&family=&part_num=&swfam=55_251&tsk=
#
# Add that MIB somewhere on your system, then tell the snmp command
# line tools to use it. I put it in /etc/snmp/mibs, then add
#
# mibs PowerNet-MIB
# mibdirs +/etc/snmp/mibs
#
# in /etc/snmp/snmp.conf
#
# Configure settings in /etc/pdu-control.conf
#
# Should probably drive SNMP direct through perl, but the command line
# tool is much easier for now...
#
# Inspired by https://tobinsramblings.wordpress.com/2011/05/03/snmp-tutorial-apc-pdus/

use strict;
use warnings;

use ConfigReader::Simple;
use Getopt::Long;
Getopt::Long::Configure ('no_ignore_case');
Getopt::Long::Configure ('no_auto_abbrev');

my $hostname;
my $comm_public;
my $comm_private;
my $base_oid = "PowerNet-MIB";
my $num_ports;
my $num_ports_read;
my @port_status;
my @ports_matched;
my $port;
my $query = "";
my $cmd = "";
my $name = "pdu-control";
my $state;

my $cmd_list = 0;
my $cmd_status = 0;
my $cmd_on = 0;
my $cmd_off = 0;
my $cmd_cycle = 0;
my $verbose = 0;
my $help = 0;

# Default - can be over-ridden in config or on the command line
my $cycletime_default = 5;
my $cycletime = $cycletime_default;
my $protective_delay = 2;
my $name_desired;

my $conf = "/etc/pdu-control.conf";
if (! -r "$conf") {
    die "Can't open config file $conf: $!\n";
}
my $config = ConfigReader::Simple->new($conf);

$hostname = $config->get("Hostname") or die "No hostname configured in $conf\n";
$comm_public = $config->get("Public") or die "No public community name configured in $conf\n";
$comm_private = $config->get("Private") or die "No private community name configured in $conf\n";
if ($config->exists("Cycletime")) {
    $cycletime = $config->get("Cycletime");
}

my $result = GetOptions("list"        => \$cmd_list,
			"status"      => \$cmd_status,
			"on"          => \$cmd_on,
			"off"         => \$cmd_off,
			"cycle"       => \$cmd_cycle,
			"cycletime=i" => \$cycletime,
			"help|h|?"    => \$help,
			"verbose|v"   => sub { $verbose += 1 })
or die ("Error parsing command line");

sub usage {
    print "\n$name\n\n";
    print "Usage: $name [--status | --on | --off | --cycle] [NAME]\n\n";
    print "Print the status of named ports on an APC PDU, or turn them on or off on demand\n\n";
    print " --list              - list the names configured on all ports\n";
    print " --status            - print the current port status of matching port(s)\n";
    print " --on                - turn power on for matching port(s)\n";
    print " --off               - turn power off for matching port(s)\n";
    print " --cycle             - turn power off, sleep a little, on for matching port(s)\n";
    print " --cycletime SECONDS - sleep for SECONDS seconds when using --cycle;\n";
    print "                       default delay is $cycletime_default\n\n";
    print "If no command is given, --status is assumed.\n\n";
    print "NAME       - a regular expresion string to match in the port name.\n";
    print "             If no NAME is given, then all ports will be matched for\n";
    print "             --status calls. For safety, --on, --off and --cycle calls must match\n";
    print "             a string to have any effect.\n";
    exit(1);
}

sub vlog {
    my $level = shift;
    my $text = shift;

    if ($verbose >= $level) {
	print "$text\n";
    }
}

if ($help) {
    usage();
}

sub power_off {
    my $port = shift;

    printf "Turning %d \"%s\" off\n",
	$port,
	$port_status[$port]{"name"};

    $query = "PowerNet-MIB::sPDUOutletCtl.$port i outletOff";
    $cmd = "snmpset -v 1 -c $comm_private $hostname $query";
    vlog (2, "Sending command \"$cmd\"");
    open (IN, "-|", "$cmd") or die "Failed to call snmpset: $!\n";
    while (defined (my $line = <IN>)) {
	chomp $line;
	print "  $line\n";
    }
    close IN;
    sleep $protective_delay; # Don't stress the PDU too much too quickly
}

sub power_on {
    my $port = shift;

    printf "Turning %d \"%s\" on\n",
	$port,
	$port_status[$port]{"name"};

    $query = "PowerNet-MIB::sPDUOutletCtl.$port i outletOn";
    $cmd = "snmpset -v 1 -c $comm_private $hostname $query";
    vlog (2, "Sending command \"$cmd\"");
    open (IN, "-|", "$cmd") or die "Failed to call snmpset: $!\n";
    while (defined (my $line = <IN>)) {
	chomp $line;
	print "  $line\n";
    }
    close IN;
    sleep $protective_delay; # Don't stress the PDU too much too quickly
}

# Validate args
if (($cmd_status + $cmd_on + $cmd_off + $cmd_cycle + $cmd_list) > 1) {
    die "Only one command allowed\n";
}

# If no command given, assume --status
if (($cmd_status + $cmd_on + $cmd_off + $cmd_cycle + $cmd_list) == 0) {
    $cmd_status = 1;
}

if ($#ARGV > 0) {
    die "@ARGV: Can only accept one name on the command line\n";
}
if ($#ARGV == 0) {
    $name_desired = $ARGV[0];
}

# Grab the number of ports
$query = "$base_oid\:\:sPDUOutletControlTableSize.0";
$cmd = "snmpget -v 1 -c $comm_public $hostname $query";
vlog (2, "Sending command \"$cmd\"");
open (IN, "-|", "$cmd") or die "Failed to call snmpget: $!\n";
while (defined (my $line = <IN>)) {
    chomp $line;
    if ($line =~ /PowerNet-MIB::sPDUOutletControlTableSize.0 = INTEGER: (\d+)/) {
	$num_ports = $1;
    }
}
close IN;

vlog (1, "Found PDU with $num_ports ports");

# Grab all the names we have associated with the ports, and the state
# of each
$query = "";
$num_ports_read = 0;
for ($port = 1; $port <= $num_ports; $port++) {
    $query = $query . "$base_oid\:\:sPDUOutletName.$port ";
}
$cmd = "snmpget -v 1 -c $comm_public $hostname $query";
vlog (2, "Sending command \"$cmd\"");
open (IN, "-|", "$cmd") or die "Failed to call snmpget: $!\n";
while (defined (my $line = <IN>)) {
    chomp $line;
#    print "$line\n";
    if ($line =~ /PowerNet-MIB::sPDUOutletName.(\d+) = STRING: \"(.*)\"/) {
	$port = $1;
	$name = $2;
	$num_ports_read++;
	$port_status[$port]{"name"} = $name;
	if (defined ($name_desired) && (length($name_desired) > 1)) {
	    if ($name =~ /^$name_desired$/) {
		push @ports_matched, $port;
	    }
	} else {
	    # ONLY empty-match ports if we're doing list or
	    # status. Don't match ALL ports for on or off or cycle!
	    if ($cmd_status or $cmd_list) {
		push @ports_matched, $port;
	    }
	}
    }
}
close IN;
vlog (1, "  Read names for $num_ports ports");

if ($cmd_list) {
    foreach $port (@ports_matched) {
	if ($port_status[$port]{"name"} =~ /\S+/) {
	    printf "\"%s\"\n",
		$port_status[$port]{"name"},
	}
    }
    exit(0);
}

$query = "";
$num_ports_read = 0;
for ($port = 1; $port <= $num_ports; $port++) {
    $query = $query . "$base_oid\:\:sPDUOutletCtl.$port ";
}
$cmd = "snmpget -v 1 -c $comm_public $hostname $query";
vlog (2, "Sending command \"$cmd\"");
open (IN, "-|", "$cmd") or die "Failed to call snmpget: $!\n";
while (defined (my $line = <IN>)) {
    chomp $line;
#    print "$line\n";
    if ($line =~ /PowerNet-MIB::sPDUOutletCtl.(\d+) = INTEGER: (.*)/) {
	$port = $1;
	$state = $2;
	$num_ports_read++;
	$port_status[$port]{"state"} = $state;
    }
}
close IN;
vlog (1, "  Read status for $num_ports ports");

if ($cmd_status) {
    vlog (1, ("--status: Matched " . scalar(@ports_matched) . " ports"));
    foreach $port (@ports_matched) {
	printf "Port %d: \"%s\" %s\n",
	    $port,
	    $port_status[$port]{"name"},
	    $port_status[$port]{"state"};
    }
}

if ($cmd_off) {
    print "--off: Matched " . scalar(@ports_matched) . " ports\n";    
    foreach $port (@ports_matched) {
	power_off($port);
    }
}

if ($cmd_on) {
    print "--on: Matched " . scalar(@ports_matched) . " ports\n";    
    foreach $port (@ports_matched) {
	power_on($port);
    }
}

if ($cmd_cycle) {
    print "--cycle: Matched " . scalar(@ports_matched) . " ports\n";    
    foreach $port (@ports_matched) {
	power_off($port);
	print "Sleeping $cycletime seconds\n";
	sleep($cycletime - $protective_delay);
	power_on($port);
    }
}

