#!/usr/bin/perl -w

use strict;
use GD;
use DateTime;
use CGI;

my @bounds;
my $im;
my $date_string;

# work out the time to go
my $end_date = DateTime->new( year      => 2011,
                              month     => 9,
                              day       => 10,
                              hour      => 13,
                              minute    => 0,
                              second    => 0,
                              time_zone => 'Europe/London');
$end_date->set_time_zone('UTC');

my $now = DateTime->now;
$now->set_time_zone('UTC');

my $q = CGI->new;

if (-1 == DateTime->compare_ignore_floating($end_date, $now)) {
    # date has already passed, say so
    $date_string = "The wedding has already happened.";
} else {
    my $duration = $end_date->subtract_datetime_absolute($now);
    my ($days, $hours, $minutes, $secs);
    my $time_string;

    $secs = $duration->seconds();

    $days = int($secs / 86400);
    if ($days) {
        if (1 == $days) {
            $time_string = "$days day";
        } else {
            $time_string = "$days days";
        }
    }
    $secs -= ($days * 86400);

    $hours = int($secs / 3600);
    if ($hours) {
        if (1 == $hours) {
            $time_string = "$time_string $hours hour";
        } else {
            $time_string = "$time_string $hours hours";
        }
    }
    $secs -= ($hours * 3600);
    
    $minutes = int($secs / 60);
    if ($minutes) {
        if (1 == $minutes) {
            $time_string = "$time_string $minutes minute";
        } else {
            $time_string = "$time_string $minutes minutes";
        }
    }
    $secs -= ($minutes * 60);

#    $time_string = "$time_string $secs seconds";
    $date_string = "The wedding will start in $time_string.";
}

# Work out how big the image needs to be
@bounds=GD::Image->stringFT(0,'./chanui__.ttf',20,0,2,40, $date_string);

if (!@bounds) {
    print STDERR "$0: unable to calculate bounding box: $@\n";
    exit 0;
}

my $x_size = abs($bounds[4] - $bounds[0]);
my $y_size = abs($bounds[5] - $bounds[1]);

$im = new GD::Image(20 + $x_size, 2*$y_size);
# allocate some colors
my $black = $im->colorAllocate(0,0,0);
my $ivory = $im->colorAllocate(255,255,176);

# make the background transparent
$im->transparent($ivory);

# And fill it
$im->filledRectangle(0,0,20 + $x_size, 2*$y_size,$ivory);

@bounds=$im->stringFT($black,'./chanui__.ttf',20,0,10,$y_size, $date_string);

print $q->header(
    -type    => 'image/png',
    -expires => '+10s',
    );

# make sure we are writing to a binary stream
binmode STDOUT;

# Convert the image to PNG and print it on standard output
print $im->png;
