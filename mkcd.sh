#!/bin/sh -e

INCUE="$1"
OUTNAME="$2"

if [ "$INCUE"x = ""x ] || [ "$OUTNAME"x = ""x ] ; then
        echo "Missing parameters"
        echo "USAGE: $0: <input cue file name> <output file name stem>"
        exit 1
fi

OUTCUE=${OUTNAME}.cue

BASEDIR=`dirname $INCUE`
FIRST_TRACK=`awk '/TRACK/ {print $2; exit}' $INCUE`
LAST_TRACK=`awk '/TRACK/ {track=$2} ; END {print track}' $INCUE`
echo "Tracks $FIRST_TRACK to $LAST_TRACK desired"

IN_TRACK=$FIRST_TRACK
while [ $IN_TRACK -le $LAST_TRACK ] ; do
        for file in ${BASEDIR}/*.flac; do
                file_title=`metaflac --show-tag=Title $file | cut -c7-`
                if [ "$file_title"x = 
    if [ -e ${BASEDIR}/${IN_TRACK}.*.flac ] ; then
        FLACFILE=${BASEDIR}/${IN_TRACK}.*.flac
    elif [ -e ${BASEDIR}/0${IN_TRACK}.*.flac ] ; then
        FLACFILE=${BASEDIR}/0${IN_TRACK}.*.flac
    else
        echo "Can't find FLAC file for track $IN_TRACK"
        exit 1
    fi
    FILES_WANTED="$FILES_WANTED $FLACFILE"
    IN_TRACK=$(($IN_TRACK + 1))
done

sox $FILES_WANTED ${OUTNAME}.wav
cp $INCUE $OUTCUE

echo "Now to write the disc:"
echo "  wodim dev=/dev/cdrw -v -pad -eject -cuefile=dummy.cue -dao"
