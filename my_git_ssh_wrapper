#!/usr/bin/perl
#
# ssh wrapper to use the correct ssh identity file for git acccess,
# depending on the repo etc.

use strict;
use warnings;
use Data::Dumper;

my $verbose = 0;
if ($ENV{'GIT_SSH_VERBOSE'}) {
    $verbose = $ENV{'GIT_SSH_VERBOSE'};
}
my $real_ssh = "/usr/bin/ssh";
my $hostname = $ENV{'HOSTNAME'};
my @known_commands = ("git-receive-pack", "git-upload-pack", "git-lfs-authenticate");
my %server_mapping = (
    "git\@github.com" => "github",
    "git\@gitlab.com" => "gitlab",
    "gitosis\@git.einval.com" => "einval",
    "gitosis\@git-dotfiles" => "einval"
#    "git\@salsa.debian.org" => "salsa"
);
my %repo_patterns = (
    "pexip/" => "pexip",
);
my @extra_args = ();
my $keyfile;
my $default_key;
my $server = "unknown";
my $repo_owner = "unknown";

if ($verbose > 1) {
    push(@extra_args, '-vvv');
}

sub debug {
    if ($verbose) {
	print STDERR "$0: @_\n";
    }
}

sub lookup_key_file {
    my $hostname = shift;
    my $server = shift;
    my $repo_owner = shift;

    my $keyfile;

    debug("checking for specific key for hostname $hostname, server $server, owner $repo_owner");

    my $ssh_dir = $ENV{'HOME'} . "/.ssh";
    if (! -d $ssh_dir) {
	debug("~/.ssh is not a dir, abandon");
	return $keyfile;
    }

    opendir(my $dir, $ssh_dir) or return $keyfile;
    while (my $entry = readdir $dir) {
	if ($entry =~ m/$hostname/
	    and $entry =~ m/$server/
	    and $entry =~ m/$repo_owner/
	    and $entry !~ m/\.pub$/) {
	    $keyfile = "$ssh_dir/$entry";
	    debug("found a specific key in $keyfile");
	    last;
	}
    }
    close $dir;
    return $keyfile;
}

my $user_host = shift;
my $remote_command = shift;
my @remote = split(/ /,$remote_command);
my $length = scalar(@remote);
debug("called on host $hostname");
debug("user_host is $user_host");
debug("remote command is @remote");

if (! grep {/^\Q$remote[0]\E$/} @known_commands) {
    die "$0: Got unexpected git verb \"$remote[0]\"\n";
}
if ($server_mapping{$user_host}) {
    $server = $server_mapping{$user_host};
}

debug("server is $server");

foreach my $pattern (keys %repo_patterns) {
    debug("Comparing $remote[1] to $pattern\n");
    if ($remote[1] =~ m(\Q$pattern\E)) {
	$repo_owner = $repo_patterns{$pattern};
	debug("repo_owner is $repo_owner");
	last;
    }
}

# Now we have all the information, yay! Try to force use of a specific
# key if we have one.
if ($hostname eq "lump" and $server eq "github") {
    debug("Running on $hostname, setting up default key");
    $default_key = "/home/steve/.ssh/lump_pexip_github_rsa";
}

# Try to use a specific key
$keyfile = lookup_key_file($hostname, $server, $repo_owner);

if (!defined $keyfile) {
    debug("Could not find specific keyfile, falling through");
    if (defined ($default_key)) {
	$keyfile = $default_key;
    }
}
if (defined $keyfile) {
    print STDERR "$0: Using keyfile $keyfile\n";
    push (@extra_args, "-o", "IdentitiesOnly=yes");
    push (@extra_args, "-i", "$keyfile");
}

my @cmd;
push(@cmd, $real_ssh);
push(@cmd, @extra_args);
push(@cmd, $user_host);
push(@cmd, $remote_command);
debug("About to exec " . Dumper(@cmd));
exec @cmd;
